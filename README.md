# TypeScript Game Engine

Began this TypeScript Game Engine as a method of learning about and trying new patterns for both WebGL and game engine development.

## Inspiration

Many of the patterns utilized are heavily inspired by either the Unity3D game engine or by the book Game Programming Design Patterns.

## 