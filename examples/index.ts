// import {
// 	Main,
// 	Scene
// } from '../src';

// new Main().setScene(new Scene({
// 	entities: [
// 		{
// 			instance: 'Entity',
// 			components: [
// 				{
// 					id
// 				}
// 			]
// 		}
// 	]
// }));

import {
	Vector, Vector3
} from '../src/math';

import {
	WebGLRenderer
} from '../src/render/index';

console.log(Vector3.distance(new Vector3(3, 2, 3), new Vector3(1, 2, 3)));
console.log(Vector3.magnitude(new Vector3(3, 2, 3)));

const renderer = new WebGLRenderer();
renderer.initialize(document.querySelector('canvas') as HTMLCanvasElement);
renderer.render();