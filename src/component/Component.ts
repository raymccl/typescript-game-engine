export interface SerializedComponent {
	id: number;
}

/**
 * @category Component
 */
export abstract class Component implements SerializedComponent {
	id!: number;

	update?(): void;
	fixedUpdate?(): void;
	start?(): void;
	end?(): void;

	serialize () {
		const serializedComponent = {
			id: this.id
		};

		return serializedComponent;
	}
}