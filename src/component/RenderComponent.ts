import { Component } from './Component';
import { Mesh } from '$/mesh';
import { Material } from '$/material';

/**
 * @category Component
 */
export class RenderComponent extends Component {
	mesh!: Mesh;
	materials!: Material[];
}