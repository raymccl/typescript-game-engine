import { Component } from './Component';
import { Vector, Quaternion } from '$/math';

/**
 * @category Component
 */
export class TransformComponent extends Component {
	position!: Vector;
	rotation!: Quaternion;
	scale!: Vector;
}