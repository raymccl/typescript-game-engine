import {
	Scene,
	SerializedScene
} from '$/scene';
import { LoadException } from '$/exception';
import { Debug } from '$/util';
import { Time } from './Time';
import { Renderer, WebGLRenderer } from '$/render';


export class Main {
	static scene: Scene;
	renderer: Renderer = new WebGLRenderer();

	public setScene (scene: SerializedScene) {
		Main.loadScene(scene);
	}

	public static start () {
		Time.addListener(this.update);
		Time.start();
	}

	private static update () {
		Main.scene.update();
	}

	private static loadScene (scene: SerializedScene) {
		try {
			Main.scene = new Scene(scene);
		} catch (e) {
			if (e instanceof LoadException) {
				Debug.log(e);
			}
		}
	}
}