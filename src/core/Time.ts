import { Timer } from '$/util';

const SIXTY_FPS = 1.6;
const THIRTY_FPS = 0.8;

export const Time = new Timer(SIXTY_FPS);