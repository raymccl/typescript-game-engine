import { Component, SerializedComponent } from '$/component';

export interface SerializedEntity {
	id: number;
	instance: string;
	components: SerializedComponent[];
}

export abstract class Entity implements SerializedEntity {
	id!: number;
	instance: string = this.constructor.name;
	components: Component[] = [];

	public update = () => {
		for (const component of this.components) {
			if (component.update) {
				component.update();
			}
		}
	}

	public hasComponent = (component: Component) => {
		return this.components.indexOf(component) > -1;
	}

	public addComponent = (component: Component) => {
		if (!this.hasComponent(component)) {
			this.components.push(component);

			if (component.start) {
				component.start();
			}
		}
	}

	public removeComponent = (component: Component) => {
		const componentIndex = this.components.indexOf(component);

		if (componentIndex > -1) {
			this.components.splice(componentIndex, 1);
		}
	}

	// public getComponent = (): Component => {
	// 	// TODO: Figure out how to find the index of a component
	// 	const componentIndex = this.components.indexOf();
	// 	let component;

	// 	if (componentIndex > -1) {
	// 		component = this.components[componentIndex];
	// 	}

	// 	return component;
	// }

	public serialize () : SerializedEntity {
		const serializedData : SerializedEntity = {
			id: this.id,
			instance: this.instance,
			components: []
		};

		for (const component of this.components) {
			serializedData.components.push(component.serialize());
		}

		return serializedData;
	}
}