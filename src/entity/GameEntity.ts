import { Entity } from './Entity';
import { TransformComponent } from '$/component';

export class GameEntity extends Entity {
	transform: TransformComponent = new TransformComponent();
	children: GameEntity[] = [];

	/**
	 * Checks if the provided [GameEntity] is in the children list
	 */
	public hasChild (GameEntity: GameEntity) {
		return this.children.indexOf(GameEntity) > -1;
	}

	/**
	 * Adds the provided [GameEntity] to the children if it isn't already a child
	 */
	public addChild (GameEntity: GameEntity) {
		if (!this.hasChild(GameEntity)) {
			this.children.push(GameEntity);
		}

		return this.children;
	}

	/**
	 * Removes the provided [GameEntity] if it exists as a child and returns the updated children list
	 * @param GameEntity the object to remove
	 */
	public removeChild (GameEntity: GameEntity): GameEntity[] {
		const childIndex = this.children.indexOf(GameEntity);

		if (childIndex > -1) {
			this.children.splice(childIndex, 1);
		}

		return this.children;
	}
}