/**
 * TODO: Review LogType options
 */
export enum LogType {
	ERROR = 'Error',
	WARNING = 'Warning',
	ASSERT = 'Assert',
	LOG = 'Log',
}