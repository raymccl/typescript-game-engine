import { Shader } from '$/shader';

export abstract class Material {
	shader!: Shader;
}