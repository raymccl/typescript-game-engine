/**
 * Base Vector class
 */
export abstract class Vector {
	protected _pos!: number[];

	public pos() { return this._pos };

	public add = (vector: Vector) => {
		this._pos.map((val, i) => val + vector._pos[i]);

		return this;
	}

	public subtract = (vector: Vector) => {
		this._pos.map((val, i) => val - vector._pos[i]);

		return this;
	}

	public divide = (vector: Vector) => {
		this._pos.map((val, i) => val / vector._pos[i]);

		return this;
	}

	public multiply = (vector: Vector) => {
		this._pos.map((val, i) => val * vector._pos[i]);

		return this;
	}

	public multiplyScalar = (scalar: number) => {
		this._pos.map(val => val * scalar);

		return this;
	}

	public divideScalar = (scalar: number) => {
		return this.multiplyScalar(1 / scalar);
	}

	public static normalize = (vector: Vector) : Vector => {
		const magnitude = Vector.magnitude(vector);

		return vector.divideScalar(magnitude || 1);
	}

	public static magnitude = (vector: Vector) : number => {
		let total = 0;

		for (let i = 0; i < vector._pos.length; i++) {
			total += vector._pos[i] * vector._pos[i];
		}

		return Math.sqrt(total);
	}

	public static distance = (fromVector: Vector, toVector: Vector) : number => {
		let distance: number = 0;

		for (let i = 0; i < fromVector._pos.length; i++) {
			if (toVector._pos[i]) {
				let diff = fromVector._pos[i] - toVector._pos[i];
				distance += diff * diff;
			}
		}

		return Math.sqrt(distance);
	}
}