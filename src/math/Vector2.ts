import { Vector } from './Vector';
import { exists } from '../util/exists';

export class Vector2 extends Vector {
	protected _pos: [
		/** X Position */
		number,
		/** Y Position */
		number
	];

	static left = new Vector2(-1, 0);
	static right = new Vector2(1, 0);
	static down = new Vector2(0, -1);
	static up = new Vector2(0, 1);

	constructor (x: number, y: number) {
		super();
		this._pos = [
			exists(x) ? x : 0,
			exists(y) ? y : 0
		];
	}

	public x() { return this._pos[0]; }
	public y() { return this._pos[1]; }
}