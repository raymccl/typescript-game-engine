import { Vector } from './Vector';

/**
 * Vector3 implementation
 */
export class Vector3 extends Vector {
	protected _pos: [
		/** X Position */
		number,
		/** Y Position */
		number,
		/** Z Position */
		number
	];

	static zero: Vector3 = new Vector3(0, 0, 0);
	static forward: Vector3 = new Vector3(0, 0, 1);
	static backward: Vector3 = new Vector3(0, 0, -1);
	static up: Vector3 = new Vector3(0, 1, 0);
	static down: Vector3 = new Vector3(0, -1, 0);
	static left: Vector3 = new Vector3(-1, 0, 0);
	static right: Vector3 = new Vector3(1, 0, 0);

	constructor (x: number, y: number, z: number) {
		super();
		this._pos = [x, y, z];
	}

	public x() { return this._pos[0]; }
	public y() { return this._pos[1]; }
	public z() { return this._pos[2]; }
}