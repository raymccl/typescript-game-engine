import { Vector3 } from '$/math';

export abstract class Mesh {
	points!: Vector3[];
	triangles!: number[];
}