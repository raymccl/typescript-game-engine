export abstract class Renderer {
	abstract initialize (canvas: HTMLCanvasElement): void;
	abstract add (): void;
	abstract remove (): void;
	abstract render (): void;
}
