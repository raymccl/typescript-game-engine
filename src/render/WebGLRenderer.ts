import { Renderer } from './Renderer';
import { WebGLException } from '$/exception/WebGLException';

export class WebGLRenderer extends Renderer {
	/**
	 * Canvas element to render the WebGL content to
	 */
	private canvas!: HTMLCanvasElement;

	public initialize (canvas: HTMLCanvasElement) {
		this.canvas = canvas;
	}

	public add () {

	}

	public remove () {

	}

	public render () {
		const gl = this.canvas.getContext('webgl');

		if (gl === null) {
			throw new WebGLException();
		}

		gl.clearColor(0.0, 0.0, 0.9, 1.0);
		gl.clear(gl.COLOR_BUFFER_BIT);
	}
}