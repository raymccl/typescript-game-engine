import { Entity, SerializedEntity } from '$/entity';

export interface SerializedScene {
	id: number;
	entities: SerializedEntity[];
}

export class Scene implements SerializedScene {
	id!: number;
	entities: Entity[] = [];

	constructor (scene?: SerializedScene) {
		if (scene) {
			this.load(scene);
		}
	}

	load (scene: SerializedScene) {
		// TODO: Load Scene
	}

	update () {
		// TODO: Implement Scene Updating
		console.log('Updating');
	}

	serialize () {
		const serializedData = [];

		for (const entity of this.entities) {
			serializedData.push(entity.serialize());
		}

		return serializedData;
	}

	addEntity (entity: Entity) {
		this.entities.push(entity);
	}
}