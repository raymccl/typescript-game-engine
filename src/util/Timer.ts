import { exists } from './exists';

/**
 * 
 */
export class Timer {
	/**
	 * Rate the timer runs at
	 */
	private rate: number = 1;
	/**
	 * 
	 */
	private interval!: NodeJS.Timeout;
	/**
	 * 
	 */
	private listeners: Function[] = [];

	constructor (rate?: number) {
		if (exists(rate)) {
			this.rate = rate;
		}
	}

	/**
	 * 
	 * @param func 
	 */
	public addListener (func: Function) {
		this.listeners.push(func);
	}

	/**
	 * 
	 */
	public update () {
		this.listeners.forEach(listener => listener());
	}

	/**
	 * 
	 */
	public start() {
		clearInterval(this.interval);
		this.interval = setInterval(this.update, this.rate);
	}

	public stop() {
		clearInterval(this.interval);
	}
}