/**
 * Returns whether the value has been defined
 * @param val
 */
export function exists(val?: Defined): val is Defined {
	return val !== null;
}